"""
Inverter1
========

"""

from sal.design_base import DesignBase
from .params import inverter1_params
from .layout import layout as inverter1_layout

class design(DesignBase):
    def __init__(self, *arg):
        DesignBase.__init__(self)
        self.params = self.parameter_class().defaults(min_lch=self.min_lch)

    @property
    def package(self):
        return "inverter1_gen"

    @classmethod
    def layout_generator_class(cls):
        """Return the layout generator class"""
        return inverter1_layout

    @classmethod
    def parameter_class(cls):
        """Return the parameter class"""
        return inverter1_params

    # Define template draw and schematic parameters below using property decorators:
    @property
    def params(self) -> inverter1_params:
        return self._params

    @params.setter
    def params(self, val: inverter1_params):
        self._params = val
